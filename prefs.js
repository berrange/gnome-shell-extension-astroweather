/*
 *  Astronomical Weather extension for GNOME Shell
 *
 *  - Creates a widget to set the preferences of the weather extension
 *
 * Copyright (C) 2014
 *
 *     Daniel P. Berrange <dan@berrange.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

const Gtk = imports.gi.Gtk;
const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const GtkBuilder = Gtk.Builder;
const Gio = imports.gi.Gio;
const GWeather = imports.gi.GWeather;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const AstroWeatherConfig = Me.imports.config;

const AstroWeatherPrefsWidget = new GObject.Class(
    {
        Name: "AstroWeatherExtension.Prefs.Widget",
        GTypeName: "AstroWeatherExtensionPrefsWidget",
        Extends: Gtk.Box,

        _init: function(params)
        {
            let that = this;

            this.parent(params);

            this.config = new AstroWeatherConfig.AstroWeatherConfig();
            this.configID = this.config.notify(function(){ that.refresh_config() });

            this.inrefresh = false;
            this.initialize_ui();
            this.refresh_config();
        },

        log : function(str)
        {
            if (this.config.debug)
                global.log(str);
        },

        initialize_ui : function()
        {
            let that = this;

            this.log("Initialize");
            this.builder = new Gtk.Builder();
            this.builder.add_from_file(Me.dir.get_path() + "/prefs.ui");

            this.widget = this.builder.get_object("prefs");
            this.locations = this.builder.get_object("locations");
            this.position = this.builder.get_object("position");
            this.debug = this.builder.get_object("debug");
            this.store = this.builder.get_object("locations-store");
            this.selection = this.builder.get_object("locations-selection");
            this.addbtn = this.builder.get_object("add");
            this.removebtn = this.builder.get_object("remove");

            this.addbtn.connect("clicked", function() { that.add_location(); });
            this.removebtn.connect("clicked",function() { that.remove_location(); });
            this.selection.connect("changed", function(sel) { that.change_location(sel); });
            this.position.connect("changed", function() { that.change_position(); });
            this.debug.connect("notify::active", function() { that.change_debug(); });

            this.locations.set_model(this.store);

            let column = new Gtk.TreeViewColumn()
            let renderer = new Gtk.CellRendererText();

            let n = this.locations.append_column(column);

            column.pack_start(renderer, null);

            column.set_cell_data_func(renderer, function(col, renderer, model, iter) {
                //that.log("Model " + model.get_value(iter, 0));
                renderer.markup = model.get_value(iter, 0);
            });

            this.add(this.widget);
        },

        refresh_config : function()
        {
            this.inrefresh = true;
            let locations = this.config.locations;

            this.log("Refresh");
            this.remove.sensitive = locations.length > 0;

            this.store.clear();
            for(let i = 0; i < locations.length; i++)
            {
                let row = this.store.append();
                let location = locations[i];
                this.log("Refresh location " + location.get_city_name());
                this.store.set_value(row, 0, location.get_city_name());
            }

            if (this.config.location >= 0) {
                let path = Gtk.TreePath.new_from_string(String(this.config.location));
                this.log("Path is now " + path + " " + path.to_string() + " " + this.config.location);
                this.locations.get_selection().select_path(path);
            }

            this.log("Refresh position " + this.config.position);
            this.position.active_id = "" + this.config.position;
            this.log("Refresh debug " + this.config.debug);
            this.debug.active = this.config.debug;

            this.inrefresh = false;
        },

        change_location : function(sel)
        {
            if (this.inrefresh) {
                return;
            }
            let path = sel.get_selected_rows(this.store)[0][0];

            if (typeof path != "undefined") {
                let newvalue = parseInt(path.to_string());
                this.log("Change location " + this.config.location + " new " + newvalue);
                if (this.config.location != newvalue) {
                    this.config.location = newvalue;
                }
            }
        },

        add_location : function()
        {
            let that = this;

            if (typeof(this.addlocation) == "undefined") {
                this.addlocation = this.builder.get_object("add-location");
                this.addlocation.set_transient_for(this.get_toplevel());
                let box = this.builder.get_object("location-box");
                this.locationentry = GWeather.LocationEntry.new(this.config.weather);
                box.add(this.locationentry);
            }

            this.addlocation.connect("response",function(w, response_id)
                                     {
                                         let location = that.locationentry.get_location();
                                         if (response_id == Gtk.ResponseType.OK && location)
                                         {
                                             let locations = that.config.locations;
                                             locations.push(location);
                                             that.config.locations = locations;
                                             if (that.config.location == -1) {
                                                 that.config.location = 0;
                                             }
                                         }
                                         that.locationentry.set_location(null);
                                         that.addlocation.hide();
                                         return 0;
                                     });

            this.addlocation.show_all();
        },

        remove_location : function()
        {
            let that = this;
            let locations = this.config.locations;
            let location = locations[this.config.location];
            if (!locations.length)
                return 0;
            let ac = this.actual_location;
            let msg = "Remove " + location.get_city_name() + " ?";

            let dialog = this.builder.get_object("delete-confirm");
            dialog.set_markup(msg);
            dialog.set_transient_for(this.get_toplevel());

            dialog.connect("response",function(w, response_id)
                           {
                               if (response_id == Gtk.ResponseType.YES)
                               {
                                   for (let i = 0; i < locations.length; i++)
                                   {
                                       if (locations[i].equal(location))
                                       {
                                           locations.splice(i, 1);
                                           break;
                                       }
                                   }
                                   that.config.locations = locations;
                                   if (locations.length > 0) {
                                       that.config.location = 0;
                                   } else {
                                       that.config.location = -1;
                                   }
                               }
                               dialog.hide();
                               return 0;
                           });

            dialog.show_all();
            return 0;
        },

        change_position : function()
        {
            if (this.inrefresh) {
                return;
            }

            let newval = parseInt(this.position.active_id);
            this.log("New " + newval + " old " + this.config.position);
            if (newval != this.config.position) {
                this.config.position = newval;
            }
        },

        change_debug : function()
        {
            if (this.inrefresh) {
                return;
            }

            if (this.debug.active != this.config.debug) {
                this.config.debug = this.debug.active;
            }
        },
    });

function init()
{
}

function buildPrefsWidget()
{
    let widget = new AstroWeatherPrefsWidget();
    widget.show_all();
    return widget;
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 *  tab-width: 8
 * End:
 */
