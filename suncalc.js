/*
 *  Astronomical Weather extension for GNOME Shell
 *
 *   - Calculates sun/moon positions / phases
 *
 * Copyright (C) 2014
 *
 *     Daniel P. Berrange <dan@berrange.com>
 *
 * Code derived from https://github.com/mourner/suncalc
 *
 *   Copyright (c) 2013, Vladimir Agafonkin
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice, this list of
 *      conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright notice, this list
 *      of conditions and the following disclaimer in the documentation and/or other materials
 *      provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

const Lang = imports.lang;


const SunCalc = new Lang.Class(
    {
        Name: "SunCalc",

        // shortcuts for easier to read formulas
        rad : Math.PI / 180,

        // sun calculations are based on http://aa.quae.nl/en/reken/zonpositie.html formulas

        // date/time constants and conversions

        dayMs : 1000 * 60 * 60 * 24,
        J1970 : 2440588,
        J2000 : 2451545,

        toJulian: function (date)
        {
            return date.valueOf() / this.dayMs - 0.5 + this.J1970;
        },

        fromJulian: function(j)
        {
            return new Date((j + 0.5 - this.J1970) * this.dayMs);
        },

        toDays: function(date)
        {
            return this.toJulian(date) - this.J2000;
        },


        // general calculations for position
        e : Math.PI / 180 * 23.4397, // obliquity of the Earth

        getRightAscension: function(l, b)
        {
            return Math.atan2(Math.sin(l) * Math.cos(this.e) - Math.tan(b) * Math.sin(this.e), Math.cos(l));
        },
        getDeclination: function(l, b)
        {
            return Math.asin(Math.sin(b) * Math.cos(this.e) + Math.cos(b) * Math.sin(this.e) * Math.sin(l));
        },
        getAzimuth: function(H, phi, dec)
        {
            return Math.atan2(Math.sin(H), Math.cos(H) * Math.sin(phi) - Math.tan(dec) * Math.cos(phi));
        },
        getAltitude: function(H, phi, dec)
        {
            return Math.asin(Math.sin(phi) * Math.sin(dec) + Math.cos(phi) * Math.cos(dec) * Math.cos(H));
        },
        getSiderealTime: function(d, lw)
        {
            return this.rad * (280.16 + 360.9856235 * d) - lw;
        },


        // general sun calculations

        getSolarMeanAnomaly: function(d)
        {
            return this.rad * (357.5291 + 0.98560028 * d);
        },
        getEquationOfCenter: function(M)
        {
            return this.rad * (1.9148 * Math.sin(M) + 0.02 * Math.sin(2 * M) + 0.0003 * Math.sin(3 * M));
        },
        getEclipticLongitude: function(M, C)
        {
            var P = this.rad * 102.9372; // perihelion of the Earth
            return M + C + P + Math.PI;
        },
        getSunCoords: function(d)
        {
            var M = this.getSolarMeanAnomaly(d),
            C = this.getEquationOfCenter(M),
            L = this.getEclipticLongitude(M, C);

            return {
                dec: this.getDeclination(L, 0),
                ra: this.getRightAscension(L, 0)
            };
        },


        // calculates sun position for a given date and latitude/longitude

        getPosition : function (date, lat, lng) {
            var lw  = this.rad * -lng,
            phi = this.rad * lat,
            d   = this.toDays(date),

            c  = this.getSunCoords(d),
            H  = this.getSiderealTime(d, lw) - c.ra;

            return {
                azimuth: this.getAzimuth(H, phi, c.dec),
                altitude: this.getAltitude(H, phi, c.dec)
            };
        },


        // sun times configuration (angle, morning name, evening name)

        times : [
            [-0.83, "sunrise",       "sunset"      ], /* official */
            [ -0.3, "sunriseEnd",    "sunsetStart" ],
            [   -6, "dawn",          "dusk"        ], /* civil */
            [  -12, "nauticalDawn",  "nauticalDusk"], /* nautical */
            [  -18, "nightEnd",      "night"       ], /* astronomical */
            [    6, "goldenHourEnd", "goldenHour"  ]
        ],

        // adds a custom time to the times config

        addTime : function (angle, riseName, setName) {
            times.push([angle, riseName, setName]);
        },

        // calculations for sun times

        J0 : 0.0009,

        getJulianCycle: function(d, lw) {
            return Math.round(d - this.J0 - lw / (2 * Math.PI));
        },
        getApproxTransit: function(Ht, lw, n) {
            return this.J0 + (Ht + lw) / (2 * Math.PI) + n;
        },
        getSolarTransitJ: function(ds, M, L) {
            return this.J2000 + ds + 0.0053 * Math.sin(M) - 0.0069 * Math.sin(2 * L);
        },
        getHourAngle: function(h, phi, d) {
            return Math.acos((Math.sin(h) - Math.sin(phi) * Math.sin(d)) / (Math.cos(phi) * Math.cos(d)));
        },

        // returns set time for the given sun altitude
        getSetJ: function(h, phi, dec, lw, n, M, L) {
            var w = this.getHourAngle(h, phi, dec),
            a = this.getApproxTransit(w, lw, n);

            return this.getSolarTransitJ(a, M, L);
        },


        // calculates sun times for a given date and latitude/longitude
        getTimes : function (date, lat, lng) {
            var lw  = this.rad * -lng,
            phi = this.rad * lat,
            d   = this.toDays(date),

            n  = this.getJulianCycle(d, lw),
            ds = this.getApproxTransit(0, lw, n),

            M = this.getSolarMeanAnomaly(ds),
            C = this.getEquationOfCenter(M),
            L = this.getEclipticLongitude(M, C),

            dec = this.getDeclination(L, 0),

            Jnoon = this.getSolarTransitJ(ds, M, L);


            var result = {
                solarNoon: this.fromJulian(Jnoon),
                nadir: this.fromJulian(Jnoon - 0.5)
            };

            var i, len, time, angle, morningName, eveningName, Jset, Jrise;

            for (i = 0, len = this.times.length; i < len; i += 1) {
                time = this.times[i];

                Jset = this.getSetJ(time[0] * this.rad,phi, dec, lw, n, M, L);
                Jrise = Jnoon - (Jset - Jnoon);

                result[time[1]] = this.fromJulian(Jrise);
                result[time[2]] = this.fromJulian(Jset);
            }

            return result;
        },

        // moon calculations, based on http://aa.quae.nl/en/reken/hemelpositie.html formulas

        getMoonCoords: function(d) { // geocentric ecliptic coordinates of the moon

            var L = this.rad * (218.316 + 13.176396 * d), // ecliptic longitude
            M = this.rad * (134.963 + 13.064993 * d), // mean anomaly
            F = this.rad * (93.272 + 13.229350 * d),  // mean distance

            l  = L + this.rad * 6.289 * Math.sin(M), // longitude
            b  = this.rad * 5.128 * Math.sin(F),     // latitude
            dt = 385001 - 20905 * Math.cos(M);  // distance to the moon in km

            return {
                ra: this.getRightAscension(l, b),
                dec: this.getDeclination(l, b),
                dist: dt
            };
        },

        getMoonPosition: function (date, lat, lng) {

            var lw  = this.rad * -lng,
            phi = this.rad * lat,
            d   = this.toDays(date),

            c = this.getMoonCoords(d),
            H = this.getSiderealTime(d, lw) - c.ra,
            h = this.getAltitude(H, phi, c.dec);

            // altitude correction for refraction
            h = h + this.rad * 0.017 / Math.tan(h + this.rad * 10.26 / (h + this.rad * 5.10));

            return {
                azimuth: this.getAzimuth(H, phi, c.dec),
                altitude: h,
                distance: c.dist
            };
        },


        // calculations for illuminated fraction of the moon,
        // based on http://idlastro.gsfc.nasa.gov/ftp/pro/astro/mphase.pro formulas

        getMoonFraction : function (date) {

            var d = this.toDays(date),
            s = this.getSunCoords(d),
            m = this.getMoonCoords(d),
            sdist = 149598000, // distance from Earth to Sun in km

            phi = Math.acos(Math.sin(s.dec) * Math.sin(m.dec) + Math.cos(s.dec) * Math.cos(m.dec) * Math.cos(s.ra - m.ra)),
            inc = Math.atan2(sdist * Math.sin(phi), m.dist - sdist * Math.cos(phi));

            return (1 + Math.cos(inc)) / 2;
        },

    });



const SunCalcTest = new Lang.Class({
    Name: "SunCalcTest",

    assert: function(cond, message) {
        if (!cond) {
            log("FAIL " + message);
        } else {
            log("PASS " + message);
        }
    },
    assertEqual: function(a, b) {
        this.assert(a == b, a + " == " + b);
    },
    assertNear: function(val1, val2, margin) {
        margin = margin || 1E-15;
        this.assert(Math.abs(val1 - val2) < margin, val1 + " =~ " + val2);
    },

    date: new Date("2013-03-05T00:00:00Z"),
    lat: 50.5,
    lng: 30.5,

    testTimes: {
        solarNoon: "2013-03-05T10:10:57Z",
        nadir: "2013-03-04T22:10:57Z",
        sunrise: "2013-03-05T04:34:57Z",
        sunset: "2013-03-05T15:46:56Z",
        sunriseEnd: "2013-03-05T04:38:19Z",
        sunsetStart: "2013-03-05T15:43:34Z",
        dawn: "2013-03-05T04:02:17Z",
        dusk: "2013-03-05T16:19:36Z",
        nauticalDawn: "2013-03-05T03:24:31Z",
        nauticalDusk: "2013-03-05T16:57:22Z",
        nightEnd: "2013-03-05T02:46:17Z",
        night: "2013-03-05T17:35:36Z",
        goldenHourEnd: "2013-03-05T05:19:01Z",
        goldenHour: "2013-03-05T15:02:52Z"
    },

    testGetPosition: function () {
        log("TEST: should return an object with correct azimuth and altitude for the given time and location");
        var sunPos = new SunCalc().getPosition(this.date, this.lat, this.lng);

        this.assertNear(sunPos.azimuth, -2.5003175907168385);
        this.assertNear(sunPos.altitude, -0.7000406838781611);
    },

    testGetTimes: function () {
        log("TEST: should return correct sun phases for the given date and location");
        var times = new SunCalc().getTimes(this.date, this.lat, this.lng);

        for (var i in this.testTimes) {
            let d = new Date(this.testTimes[i]).toUTCString();
            this.assertEqual(times[i].toUTCString(), d);
        }
    },

    testGetMoonPosition: function () {
        log("TEST: should return an object with correct moon position data given time and location");
        var moonPos = new SunCalc().getMoonPosition(this.date, this.lat, this.lng);

        this.assertNear(moonPos.azimuth, -0.9783999522438226);
        this.assertNear(moonPos.altitude, 0.006969727754891917);
        this.assertNear(moonPos.distance, 364121.37256256194);
    },

    testGetMoonFraction: function () {
        log("TEST: should return fraction of illuminated moon given time");
        this.assertNear(new SunCalc().getMoonFraction(this.date), 0.4848068202456373);
    },

    test: function() {
        this.testGetPosition();
        this.testGetTimes();
        this.testGetMoonPosition();
        this.testGetMoonFraction();
    },
});

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 *  tab-width: 8
 * End:
 */
