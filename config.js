/*
 *  Astronomical Weather Watch extension for GNOME Shell
 *
 *  - Records configuration information
 *
 * Copyright (C) 2014
 *
 *     Daniel P. Berrange <dan@berrange.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

const Lang = imports.lang;
const ExtensionUtils = imports.misc.extensionUtils;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const GWeather = imports.gi.GWeather;


const AstroWeatherConfig = new Lang.Class(
    {
        Name: "AstroWeatherConfig",

	SETTING_KEY_LOCATIONS: "locations",
	SETTING_KEY_LOCATION: "location",
	SETTING_KEY_POSITION: "position",
	SETTING_KEY_DEBUG: "debug",

        _init: function()
        {
	    let extension = ExtensionUtils.getCurrentExtension();
	    let schemaName = "org.gnome.shell.extensions.astroweather";

	    const GioSSS = Gio.SettingsSchemaSource;

	    let schemaDir = extension.dir.get_child('schemas');
            let schemaSrc = GioSSS.new_from_directory(schemaDir.get_path(),
						      GioSSS.get_default(),
						      false);

	    let schema = schemaSrc.lookup(schemaName, true);
	    if (!schema)
		throw new Error("Cannot find schema " + schemaName);

	    this.weather = GWeather.Location.new_world(false);
	    this.settings = new Gio.Settings({ settings_schema: schema });
	},

	notify : function(notifier)
	{
	    return this.settings.connect("changed", notifier);
	},

	unnotify: function(id)
	{
	    this.settings.disconnect(id);
	},

	get locations()
	{
            let vals = this.settings.get_value(this.SETTING_KEY_LOCATIONS).deep_unpack();
	    let locations = [];
            for (let i = 0; i < vals.length; i++) {
		locations[i] = this.weather.deserialize(vals[i]);
	    }
            return locations;
	},

	set locations(newvals)
	{
	    let locations = [];
	    for (let i = 0 ; i < newvals.length ; i++) {
		locations[i] = newvals[i].serialize();
	    }
	    this.settings.set_value(this.SETTING_KEY_LOCATIONS, new GLib.Variant("av", locations));
	},

	get location()
	{
	    return this.settings.get_int(this.SETTING_KEY_LOCATION);
	},

	set location(newval)
	{
	    this.settings.set_int(this.SETTING_KEY_LOCATION, newval);
	},

	get position()
	{
	    return this.settings.get_enum(this.SETTING_KEY_POSITION);
	},

	set position(newval)
	{
	    this.settings.set_enum(this.SETTING_KEY_POSITION, newval);
	},

	get debug()
	{
	    return this.settings.get_boolean(this.SETTING_KEY_DEBUG);
	},

	set debug(newval)
	{
	    this.settings.set_boolean(this.SETTING_KEY_DEBUG, newval);
	}

    });
