
MANIFEST = \
	astroweather.js \
	config.js \
	extension.js \
	prefs.js \
	suncalc.js \
	COPYING \
	NEWS \
	README \
	prefs.ui \
	icons \
	stylesheet.css \
	metadata.json \
	schemas/gschemas.compiled \
	schemas/*.xml

all:
	glib-compile-schemas schemas/

clean:
	rm -f *.zip schemas/gschemas.compiled

dist: all
	zip -qr gnome-shell-extension-astroweather.zip $(MANIFEST)
